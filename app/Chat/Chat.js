'use strict';

import React, {Component} from 'react';
import {Platform, StyleSheet, View, FlatList, Text, TextInput} from 'react-native';
import MessageList from './Message/MessageList';
import Composer from './InputToolBar/Composer';
import SelectPhotoButton from './InputToolBar/SelectPhotoButton';
import SelectQuestionButton from './InputToolBar/SelectQuestionButton';
import SendMessageButton from './InputToolBar/SendMessageButton';


export default class extends Component {

    static defaultProps = {
        minInputToolBarHeight: 52,
        maxInputToolBarHeight: 27 + 19 * 8,
    };

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.user}`,
        headerBackTitle: null,
    });

    constructor(props) {
        super(props);

        this.onComposerHeightShouldChange = this.onComposerHeightShouldChange.bind(this);

        this.state = {
            inputToolBarHeight: props.minInputToolBarHeight,
        };
    }

    onComposerHeightShouldChange(diff) {
        let newInputToolBarHeight = this.state.inputToolBarHeight + diff;

        if (newInputToolBarHeight < this.props.minInputToolBarHeight) {
            newInputToolBarHeight = this.props.minInputToolBarHeight;
        } else if (newInputToolBarHeight > this.props.maxInputToolBarHeight) {
            newInputToolBarHeight = this.props.maxInputToolBarHeight;
        }

        this.setState({
            inputToolBarHeight: newInputToolBarHeight,
        });
    }

    render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.messageContainer]}>
                    <MessageList/>
                </View>
                <View style={[styles.inputToolBarContainer, {height: this.state.inputToolBarHeight}]}>
                    <View style={[styles.inputToolBar]}>
                        <View style={[styles.inputToolBarButton]}>
                            <SelectPhotoButton/>
                        </View>
                        <View style={[styles.inputToolBarButton]}>
                            <SelectQuestionButton/>
                        </View>
                        <Composer onHeightShouldChange={this.onComposerHeightShouldChange}/>
                        <View style={[styles.inputToolBarButton]}>
                            <SendMessageButton/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    messageContainer: {
        flex: 1,
    },
    inputToolBarContainer: {
        borderTopWidth: 1,
        borderColor: '#ddd',
    },
    inputToolBar: {
        flex: 1,
        flexDirection: 'row',
        paddingRight: 10,
    },
    inputToolBarButton: {
        width: 36,
    },
});
