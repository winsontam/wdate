'use strict';

import React, {Component, PropTypes} from 'react';
import {Platform, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ButtonContainer from './ButtonContainer';


export default class extends Component {

    static defaultProps = {
        color: '#bbb',
        size: 26,
    };

    static propTypes = {};

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ButtonContainer>
                <Icon {...this.props} name="image-multiple"/>
            </ButtonContainer>
        );
    }
}
