'use strict';

import React, {Component, PropTypes} from 'react';
import {Platform, StyleSheet, View, TextInput} from 'react-native';


export default class extends Component {

    static defaultProps = {
        placeholder: 'Type a message...',
        placeholderTextColor: '#B2B2B2',
        multiline: true,
        enablesReturnKeyAutomatically: true,
        underlineColorAndroid: 'transparent',
    };

    static propTypes = {
        onHeightShouldChange: React.PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.onInputLayout = this.onInputLayout.bind(this);
        this.onInputContentSizeChange = this.onInputContentSizeChange.bind(this);

        this.inputHeight = null;
        this.inputContentHeight = null;
    }


    onInputLayout(event) {
        if (!this.initInputHeight) {
            this.initInputHeight = event.nativeEvent.layout.height;
        }

        this.inputHeight = event.nativeEvent.layout.height;

        if (this.props.onLayout) {
            this.props.onLayout(event);
        }
    }

    onInputContentSizeChange(event) {
        this.inputContentHeight = event.nativeEvent.contentSize.height;

        if (this.inputHeight) {
            const inputHeightDiff = this.inputContentHeight - this.inputHeight + 10;

            if (this.props.onHeightShouldChange) {
                this.props.onHeightShouldChange(inputHeightDiff);
            }
        }

        if (this.props.onContentSizeChange) {
            this.props.onContentSizeChange(event);
        }
    }

    render() {
        return (
            <View style={[styles.container]}>
                <TextInput
                    {...this.props}
                    style={[styles.input, this.props.style]}
                    onLayout={this.onInputLayout}
                    onContentSizeChange={this.onInputContentSizeChange}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 4,
        paddingLeft: 8,
        paddingRight: 8,
        margin: 10,
        marginRight: 0,
    },
    input: {
        flex: 1,
        fontSize: 16,
        lineHeight: 16,
    },
});
