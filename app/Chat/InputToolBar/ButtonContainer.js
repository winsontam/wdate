'use strict';

import React, {Component, PropTypes} from 'react';
import {Platform, StyleSheet, View, TouchableOpacity} from 'react-native';


export default class extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity style={[styles.container]}>
                {this.props.children}
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        margin: 10,
        marginRight: 0,
    },
});
