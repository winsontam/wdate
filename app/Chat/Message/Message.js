'use strict';

import React, {Component, PropTypes} from 'react';
import {Platform, StyleSheet, FlatList, View, Text} from 'react-native';


export default class extends Component {

    static defaultProps = {
        position: 'left',
    };

    static propTypes = {
        position: React.PropTypes.oneOf(['left', 'right']),
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[styles.container[this.props.position]]}>
                <View style={[styles.bubble[this.props.position]]}>
                    <View style={[styles.wrapper]}>
                        <Text>{this.props.children}</Text>
                    </View>
                </View>
            </View>
        );
    }
}


const containerStyle = {
    flex: 1,
    transform: [{scaleY: -1}],
};

const bubbleStyle = {
    borderRadius: 15,
    marginBottom: 10,
    backgroundColor: '#f0f0f0',
};

const styles = {
    container: StyleSheet.create({
        left: {
            ...containerStyle,
            alignItems: 'flex-start',
        },
        right: {
            ...containerStyle,
            alignItems: 'flex-end',
        },
    }),
    bubble: StyleSheet.create({
        left: {
            ...bubbleStyle,
            justifyContent: 'flex-end',
            marginRight: 50,
        },
        right: {
            ...bubbleStyle,
            justifyContent: 'flex-start',
            marginLeft: 50,
        },
    }),
    ...StyleSheet.create({
        wrapper: {
            padding: 10,
        },
    }),
};
