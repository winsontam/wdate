'use strict';

import React, {Component, PropTypes} from 'react';
import {Platform, StyleSheet, FlatList, View, Text} from 'react-native';
import Message from './Message';


export default class extends Component {

    constructor(props) {
        super(props);

        this.renderMessage = this.renderMessage.bind(this);

        this.state = {
            data: [
                {
                    key: 2,
                    message: 'Are you busy? :(',
                },
                {
                    key: 1,
                    message: 'Hi, nice to meet you in here, may i know more about you? how are you today? :)',
                }
            ]
        };
    }

    renderMessage({item: {message}}) {
        return (
            <Message position="left">
                {message}
            </Message>
        );
    }

    render() {
        return (
            <FlatList
                style={[styles.list]}
                data={this.state.data}
                renderItem={this.renderMessage}
            />
        );
    }
}


const styles = StyleSheet.create({
    list: {
        padding: 10,
        transform: [{ scaleY: -1 }],
    },
});
