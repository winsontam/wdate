'use strict';

import React from 'react';
import {Platform, StyleSheet, KeyboardAvoidingView} from 'react-native';
import Navigator from './Navigator/Navigator';


let App = Navigator;

if (Platform.OS === 'ios') {
    App = (props) => (
        <KeyboardAvoidingView behavior="padding" style={[styles.container]}>
            <Navigator/>
        </KeyboardAvoidingView>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});


export default App;
