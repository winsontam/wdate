'use strict';

import React from 'react';
import {StackNavigator} from 'react-navigation';
import Home from '../Home/Home';
import Chat from '../Chat/Chat';


export default StackNavigator({
    Home: {
        screen: Home,
    },
    Chat: {
        path: 'chat/:user',
        screen: Chat,
    },
});
