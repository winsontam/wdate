'use strict';

import React from 'react';
import {Button} from 'react-native';
import Chat from '../Chat/Chat';


export default class extends React.Component {
    static navigationOptions = {
        title: 'Home',
        headerBackTitle: null,
    };

    componentDidMount() {
        this.props.navigation.navigate('Chat', {user: 'Chat'});
    }

    render() {
        return (
            <Button
                onPress={() => this.props.navigation.navigate('Chat', {user: 'Chat'})}
                title="Let's chat!"
            />
        );
    }
}